#!/usr/bin/env zsh
WORKDIR=${0:h}
LOGDIR="/home/klt/log"
MSGFILE="/home/klt/.config/i3status/msg.txt"
OUTFILE="${LOGDIR}/${0:t:r}.out"
ERRFILE="${LOGDIR}/${0:t:r}.err"
SERVICES=($*)

source ${HOME}/.zshrc

exec 1>${OUTFILE} 2>${ERRFILE}

logg "START $0 $*: running as $(whoami)"

echo "" > ${MSGFILE}

function activateService {
    systemctl enable ${SERVICE}
    sleep 3
    systemctl start ${SERVICE}
    sleep 3
    if [[ ${SERVICE} == bluetooth ]]; then
        rfkill unblock ${SERVICE}
        sleep 3
        rfkill
        sleep 3
    fi
    systemctl status ${SERVICE}
    echo ""
    if [[ ${SERVICE} == bluetooth ]]; then
        echo ""
        bt-device -l
        echo ""
        logg "! You may need to switch OFF and ON your ${SERVICE} device !"
    fi
}

function deactivateService {
    logg "systemctl stop ${SERVICE}"
    systemctl stop ${SERVICE}
    sleep 3
    logg "systemctl disable ${SERVICE}"
    systemctl disable ${SERVICE}
    sleep 3
    systemctl status ${SERVICE}
}

function toggleService {
    local SERVICE=$1
    logg "SERVICE = ${SERVICE}"
    local STATUS=$(systemctl is-active ${SERVICE})
    logg "STATUS  = ${STATUS}"
    echo ""

    if [[ $STATUS == "active" ]]; then
        deactivateService
    else
        activateService
    fi

    echo ""
    logg "SERVICE = ${SERVICE}"
    STATUS=$(systemctl is-active ${SERVICE})
    logg "STATUS  = ${STATUS}"
    echo ""
    logg "${SERVICE}: ${STATUS}, " >> ${MSGFILE}
}

for svc in ${SERVICES}; do toggleService ${svc}; sleep 6 ; done

logg "DONE $0 $*"

exit $?
